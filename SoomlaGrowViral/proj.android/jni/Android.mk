
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

FILE_LIST := $(wildcard $(LOCAL_PATH)/../../Classes/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/jansson/*.c) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/Profile/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/Highway/*.cpp)  
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/rewards/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/PurchaseTypes/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/NativeImpl/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/jsb/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/domain/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/domain/virtualGoods/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/domain/virtualCurrencies/*.cpp) 
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Soomla/data/*.cpp) 

LOCAL_SRC_FILES := hellocpp/main.cpp
LOCAL_SRC_FILES += $(FILE_LIST:$(LOCAL_PATH)/%=%)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/jansson
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla/Highway
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla/Profile
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla/rewards
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla/PurchaseTypes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla/NativeImpl
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla/jsb
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla/domain
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla/domain/virtualGoods
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla/domain/virtualCurrencies
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Soomla/data

LOCAL_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += PluginProtocolStatic

include $(BUILD_SHARED_LIBRARY)
$(call import-module,.)
$(call import-module,plugin/protocols/proj.android/jni)
