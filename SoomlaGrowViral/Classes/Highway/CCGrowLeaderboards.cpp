/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCGrowLeaderboards.h"
#include "CCSoomlaUtils.h"
#include "CCNdkBridge.h"
#include "CCSoomlaMacros.h"

USING_NS_CC;

using namespace soomla;
namespace grow {
    
    #define TAG "SOOMLA CCGrowLeaderboards"
    
    bool CCGrowLeaderboards::fetchFriendsStates(int providerId, cocos2d::__Array *friendsProfileIds) {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowLeaderboards::fetchFriendsStates");
        params->setObject(__Integer::create(providerId), "providerId");
        params->setObject(friendsProfileIds, "friendsList");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call fetch friends states failed with error: %s", error->getInfo())->getCString());
            return false;
        }
        
        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);
        
        if (ret->getValue()) {
            __Bool *willStartOperation = dynamic_cast<__Bool *>(retParams->objectForKey("willStart"));
            return willStartOperation->getValue();
        }
        
        return false;
    }
}
