/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCFriendState_H_
#define __CCFriendState_H_

#include "cocos2d.h"
#include "CCSoomlaMacros.h"
#include "CCDomain.h"
#include "CCHighwayConsts.h"

namespace grow {
    
    /**
     @class CCFriendState
     @brief Represents a friend's state in the game.
     It contains all relevant information to create a leaderboard between a 
     player and his friends.
     
     Inheritance CCFriendState > CCDomain > cocos2d::Ref
     */
    class CCFriendState : public soomla::CCDomain {
        SL_SYNTHESIZE_RETAIN_WITH_DICT(cocos2d::__String *, mProfileId, ProfileId, CCHighwayConsts::JSON_FRIEND_PROFILE_ID);
        SL_SYNTHESIZE_RETAIN_WITH_DICT(cocos2d::__Dictionary *, mRecords, Records, CCHighwayConsts::JSON_FRIEND_RECORDS);
        SL_SYNTHESIZE_RETAIN_WITH_DICT(cocos2d::__Dictionary *, mLastCompletedWorlds, LastCompletedWorlds, CCHighwayConsts::JSON_FRIEND_LAST_COMPLETED_WORLDS);
    public:
        /**
         Constructor
         
         Main constructor for the gift which nullifies all information in
         the class
         */
        CCFriendState() : CCDomain(), mProfileId(NULL), mRecords(NULL), mLastCompletedWorlds(NULL) {};
        
        /**
         Creates an instance of `CCFriendState` according to the information
         provided
         
         @param profileId The profile ID of the user in the social network
         @param records a Map of worlds having levels completed in them by the 
         user. It maps between the world ID and a completed inner world ID.
         @param lastCompletedWorlds a Map of records made by the user.
         It maps between score ID and the highest record done by the user.
         @return the created instance
         */
        static CCFriendState *create(cocos2d::__String *profileId,
                                     cocos2d::__Dictionary *records,
                                     cocos2d::__Dictionary *lastCompletedWorlds);
        
        SL_CREATE_WITH_DICTIONARY(CCFriendState);
        
        /**
         Initializes the class instance with the provided information
         @param profileId The profile ID of the user in the social network
         @param records a Map of worlds having levels completed in them by the
         user. It maps between the world ID and a completed inner world ID.
         @param lastCompletedWorlds a Map of records made by the user.
         It maps between score ID and the highest record done by the user.
         @return true if the initialization was successful, false otherwise
         */
        virtual bool init(
                          cocos2d::__String *profileId,
                          cocos2d::__Dictionary *records,
                          cocos2d::__Dictionary *lastCompletedWorlds);
        
        /**
         Initializes the class instance with information provided in a
         dictionary format with keys corresponding to fields JSON constants
         (see SL_SYNTHESIZE_RETAIN_WITH_DICT macros above)
         */
        virtual bool initWithDictionary(cocos2d::__Dictionary *dict);
        
        /**
         Converts the friend state instance into a dictionary using the fields'
         JSON constants as keys
         (see SL_SYNTHESIZE_RETAIN_WITH_DICT macros above)
         */
        virtual cocos2d::__Dictionary *toDictionary();
        
        /**
         Destructor for the friend state
         */
        virtual ~CCFriendState();
    };
}

#endif //__CCFriendState_H_
