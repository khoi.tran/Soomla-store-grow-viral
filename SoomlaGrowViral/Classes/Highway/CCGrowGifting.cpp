/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCGrowGifting.h"
#include "CCNdkBridge.h"
#include "CCSoomlaUtils.h"
#include "CCSoomlaMacros.h"

USING_NS_CC;

using namespace soomla;
namespace grow {
    
#define TAG "SOOMLA CCGrowGifting"
    
    static CCGrowGifting *sInstance = nullptr;
    
    CCGrowGifting *CCGrowGifting::getInstance() {
        if (!sInstance)
        {
            sInstance = new CCGrowGifting();
            sInstance->retain();
        }
        return sInstance;
    }
    
    void CCGrowGifting::initShared() {
        CCGrowGifting *growGifting = CCGrowGifting::getInstance();
        if (!growGifting->init()) {
            exit(1);
        }
    }
    
    bool CCGrowGifting::init() {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowGifting::init");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call init failed with error: %s", error->getInfo())->getCString());
            return false;
        }
        
        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);
        
        return ret->getValue();
    }
    
    bool CCGrowGifting::sendGift(int toProvider, const char *toProfileId, const char *itemId, int amount) {
        return sendGift(toProvider, toProfileId, itemId, amount, false);
    }
    
    bool CCGrowGifting::sendGift(int toProvider, const char *toProfileId, const char *itemId, int amount, bool deductFromUser) {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowGifting::sendGift");
        params->setObject(__Integer::create(toProvider), "toProvider");
        params->setObject(__String::create(toProfileId), "toProfileId");
        params->setObject(__String::create(itemId), "itemId");
        params->setObject(__Integer::create(amount), "amount");
        params->setObject(__Bool::create(deductFromUser), "deductFromUser");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call send gift failed with error: %s", error->getInfo())->getCString());
            return false;
        }
        
        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);
        
        if (ret->getValue()) {
            __Bool *willStartOperation = dynamic_cast<__Bool *>(retParams->objectForKey("willStart"));
            return willStartOperation->getValue();
        }
        
        return false;
    }
    
    CCGrowGifting::CCGrowGifting() {
    }
}