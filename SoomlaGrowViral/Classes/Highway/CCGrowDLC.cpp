/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCGrowDLC.h"
#include "CCSoomlaUtils.h"
#include "CCNdkBridge.h"
#include "CCSoomlaMacros.h"

USING_NS_CC;

using namespace soomla;
namespace grow {
    
    #define TAG "SOOMLA CCGrowDLC"
    
    static CCGrowDLC *sInstance = nullptr;

    CCGrowDLC *CCGrowDLC::getInstance() {
        if (!sInstance)
        {
            sInstance = new CCGrowDLC();
            sInstance->retain();
        }
        return sInstance;
    }

    void CCGrowDLC::initShared() {
        CCGrowDLC *soomlaDLC = CCGrowDLC::getInstance();
        if (!soomlaDLC->init()) {
            exit(1);
        }
    }

    bool CCGrowDLC::init() {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowDLC::init");
        
        CCError *error = NULL;
        CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call init failed with error: %s", error->getInfo())->getCString());
            
            return false;
        }
        
        return true;
    }


    void CCGrowDLC::checkPackageStatus(const char *packageId) {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowDLC::checkPackageStatus");
        params->setObject(__String::create(packageId), "packageId");
        
        CCError *error = NULL;
        CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call check package status failed with error: %s", error->getInfo())->getCString());
        }
    }

    void CCGrowDLC::checkSyncedPackagesStatus() {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowDLC::checkSyncedPackagesStatus");
        
        CCError *error = NULL;
        CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call check synced packages status failed with error: %s", error->getInfo())->getCString());
        }
    }

    bool CCGrowDLC::startSync(const char *packageId) {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowDLC::startSync");
        params->setObject(__String::create(packageId), "packageId");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call start sync failed with error: %s", error->getInfo())->getCString());
            return false;
        }
        
        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);
        
        return ret->getValue();
    }

    const char *CCGrowDLC::getPathToFile(const char *packageId, const char *filename) {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowDLC::getFilePath");
        params->setObject(__String::create(packageId), "packageId");
        params->setObject(__String::create(filename), "fileName");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call get path to file failed with error: %s", error->getInfo())->getCString());
            return NULL;
        }
        
        SL_EXTRACT_FROM_RETURN(__String, ret, retParams);
        
        return ret->getCString();
    }

    cocos2d::__Array *CCGrowDLC::getListOfFiles(const char *packageId) {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowDLC::getFilesPathsInPackage");
        params->setObject(__String::create(packageId), "packageId");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call get list of files failed with error: %s", error->getInfo())->getCString());
            return NULL;
        }
        
        SL_EXTRACT_FROM_RETURN(__Array, ret, retParams);
        
        return ret;
    }
}