/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCGift.h"

grow::CCGift *grow::CCGift::create(cocos2d::__String *giftId,
                                       cocos2d::__String *fromUid,
                                       cocos2d::__Integer *toProvider,
                                       cocos2d::__String *toProfileId,
                                       CCGiftPayload *payload) {
    
    CCGift *ret = new CCGift();
    if (ret->init(giftId, fromUid, toProvider, toProfileId, payload)) {
        ret->autorelease();
    }
    else {
        CC_SAFE_DELETE(ret);
    }
    return ret;
}

bool grow::CCGift::init(cocos2d::__String *giftId,
                          cocos2d::__String *fromUid,
                          cocos2d::__Integer *toProvider,
                          cocos2d::__String *toProfileId,
                          CCGiftPayload *payload) {
    
    setId(giftId);
    setFromUid(fromUid);
    setToProvider(toProvider);
    setToProfileId(toProfileId);
    setPayload(payload);
    
    return true;
}

bool grow::CCGift::initWithDictionary(cocos2d::__Dictionary *dict) {
    fillIdFromDict(dict);
    fillFromUidFromDict(dict);
    fillToProviderFromDict(dict);
    fillToProfileIdFromDict(dict);
    fillPayloadFromDict(dict);
    
    return true;
}

void grow::CCGift::fillPayloadFromDict(cocos2d::__Dictionary *dict) {
    cocos2d::__Dictionary *payloadDict = dynamic_cast<cocos2d::__Dictionary *>(dict->objectForKey(CCHighwayConsts::JSON_GIFT_PAYLOAD));
    CC_ASSERT(payloadDict);
    
    CCGiftPayload *payload = CCGiftPayload::createWithDictionary(payloadDict);
    setPayload(payload);
}

cocos2d::__Dictionary *grow::CCGift::toDictionary() {
    cocos2d::__Dictionary* dict = cocos2d::__Dictionary::create();
    
    putIdToDict(dict);
    putFromUidToDict(dict);
    putToProviderToDict(dict);
    putToProfileIdToDict(dict);
    putPayloadToDict(dict);
    
    return dict;
}

void grow::CCGift::putPayloadToDict(cocos2d::__Dictionary *dict) {
    dict->setObject(getPayload()->toDictionary(), CCHighwayConsts::JSON_GIFT_PAYLOAD);
}

grow::CCGift::~CCGift() {
    CC_SAFE_RELEASE(mId);
    CC_SAFE_RELEASE(mFromUid);
    CC_SAFE_RELEASE(mToProvider);
    CC_SAFE_RELEASE(mToProfileId);
    CC_SAFE_RELEASE(mPayload);
}