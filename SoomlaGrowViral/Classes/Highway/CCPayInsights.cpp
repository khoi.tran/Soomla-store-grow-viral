/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCPayInsights.h"

bool grow::CCPayInsights::initWithDictionary(cocos2d::__Dictionary *dict) {
    fillPayRankByGenreFromDict(dict);
    
    return true;
}

cocos2d::__Dictionary *grow::CCPayInsights::toDictionary() {
    cocos2d::__Dictionary* dict = cocos2d::__Dictionary::create();
    
    putPayRankByGenreToDict(dict);
    
    return dict;
}

int grow::CCPayInsights::getPayRankForGenre(grow::Genre genre) {
    cocos2d::__String *key = cocos2d::__String::createWithFormat("%d", genre);
    cocos2d::Ref *res = getPayRankByGenre()->objectForKey(key->getCString());
    if (res != nullptr) {
        return dynamic_cast<cocos2d::__Integer*>(res)->getValue();
    } else {
        return -1;
    }
}

grow::CCPayInsights::~CCPayInsights() {
    CC_SAFE_RELEASE(mPayRankByGenre);
}