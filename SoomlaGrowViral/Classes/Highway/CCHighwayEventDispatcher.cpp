/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCHighwayEventDispatcher.h"
#include "CCHighwayConsts.h"
#include "CCSoomlaEventDispatcher.h"
#include "CCSoomlaMacros.h"
#include "CCGrowSync.h"
#include "CCFriendState.h"
#include "CCGrowInsights.h"

using namespace soomla;
namespace grow {
    
    USING_NS_CC;
    
    static CCHighwayEventDispatcher *s_SharedInstance = NULL;
    
    CCHighwayEventDispatcher *CCHighwayEventDispatcher::getInstance() {
        if (!s_SharedInstance) {
            s_SharedInstance = new CCHighwayEventDispatcher();
            s_SharedInstance->init();
        }
        
        return s_SharedInstance;
    }
    
    bool CCHighwayEventDispatcher::init() {
        
        CCSoomlaEventDispatcher *eventDispatcher = CCSoomlaEventDispatcher::getInstance();
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::INTERNAL_EVENT_ON_STATE_CONFLICT,
                                              [this](__Dictionary *parameters) {
                                                  __Dictionary *remoteState = dynamic_cast<__Dictionary *>(parameters->objectForKey("remoteState"));
                                                  __Dictionary *currentState = dynamic_cast<__Dictionary *>(parameters->objectForKey("currentState"));
                                                  __Dictionary *stateDiff = dynamic_cast<__Dictionary *>(parameters->objectForKey("stateDiff"));
                                                  CCGrowSync::getInstance()->resolveStateConflict(remoteState, currentState, stateDiff);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GROW_SYNC_INITIALIZED,
                                              [this](__Dictionary *parameters) {
                                                  this->onGrowSyncInitialized();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_MODEL_SYNC_STARTED,
                                              [this](__Dictionary *parameters) {
                                                  this->onModelSyncStarted();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_MODEL_SYNC_FINISHED,
                                              [this](__Dictionary *parameters) {
                                                  __Array *changedComponents = dynamic_cast<__Array *>(parameters->objectForKey("changedComponents"));
                                                  changedComponents->retain();
                                                  this->onModelSyncFinished(changedComponents);
                                                  changedComponents->release();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_MODEL_SYNC_FAILED,
                                              [this](__Dictionary *parameters) {
                                                  __Integer *errorCode = dynamic_cast<__Integer *>(parameters->objectForKey("errorCode"));
                                                  __String *errorMessage = dynamic_cast<__String *>(parameters->objectForKey("errorMessage"));
                                                  this->onModelSyncFailed(CCModelSyncError(errorCode->getValue()), errorMessage);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_STATE_SYNC_STARTED,
                                              [this](__Dictionary *parameters) {
                                                  this->onStateSyncStarted();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_STATE_SYNC_FINISHED,
                                              [this](__Dictionary *parameters) {
                                                  __Array *changedComponents = dynamic_cast<__Array *>(parameters->objectForKey("changedComponents"));
                                                  __Array *failedComponents = dynamic_cast<__Array *>(parameters->objectForKey("failedComponents"));
                                                  
                                                  changedComponents->retain();
                                                  failedComponents->retain();
                                                  
                                                  this->onStateSyncFinished(changedComponents, failedComponents);
                                                  
                                                  changedComponents->release();
                                                  failedComponents->release();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_STATE_SYNC_FAILED,
                                              [this](__Dictionary *parameters) {
                                                  __Integer *errorCode = dynamic_cast<__Integer *>(parameters->objectForKey("errorCode"));
                                                  __String *errorMessage = dynamic_cast<__String *>(parameters->objectForKey("errorMessage"));
                                                  this->onStateSyncFailed(CCStateSyncError(errorCode->getValue()), errorMessage);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_STATE_RESET_STARTED,
                                              [this](__Dictionary *parameters) {
                                                  this->onStateResetStarted();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_STATE_RESET_FINISHED,
                                              [this](__Dictionary *parameters) {
                                                  this->onStateResetFinished();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_STATE_RESET_FAILED,
                                              [this](__Dictionary *parameters) {
                                                  __Integer *errorCode = dynamic_cast<__Integer *>(parameters->objectForKey("errorCode"));
                                                  __String *errorMessage = dynamic_cast<__String *>(parameters->objectForKey("errorMessage"));
                                                  this->onStateResetFailed(CCStateSyncError(errorCode->getValue()), errorMessage);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GROW_GIFTING_INITIALIZED,
                                              [this](__Dictionary *parameters) {
                                                  this->onGrowGiftingInitialized();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GIFTS_RETRIEVE_STARTED,
                                              [this](__Dictionary *parameters) {
                                                  this->onGiftsRetrieveStarted();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GIFTS_RETRIEVE_FINISHED,
                                              [this](__Dictionary *parameters) {
                                                  __Array *retrievedGiftDicts = dynamic_cast<__Array *>(parameters->objectForKey("givenGifts"));
                                                  __Array *retrievedGifts = __Array::create();
                                                  
                                                  __Dictionary *giftDict = NULL;
                                                  for (unsigned int i = 0; i < retrievedGiftDicts->count(); i++) {
                                                      giftDict = dynamic_cast<__Dictionary *>(retrievedGiftDicts->getObjectAtIndex(i));
                                                      CC_ASSERT(giftDict);
                                                      
                                                      CCGift *gift = CCGift::createWithDictionary(giftDict);
                                                      retrievedGifts->addObject(gift);
                                                  }
                                                  
                                                  this->onGiftsRetrieveFinished(retrievedGifts);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GIFTS_RETRIEVE_FAILED,
                                              [this](__Dictionary *parameters) {
                                                  __String *errorMessage = dynamic_cast<__String *>(parameters->objectForKey("errorMessage"));
                                                  this->onGiftsRetrieveFailed(errorMessage);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GIFT_SEND_STARTED,
                                              [this](__Dictionary *parameters) {
                                                  __Dictionary *giftDict = dynamic_cast<__Dictionary *>(parameters->objectForKey("gift"));
                                                  CCGift *gift = CCGift::createWithDictionary(giftDict);
                                                  this->onGiftSendStarted(gift);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GIFT_SEND_FINISHED,
                                              [this](__Dictionary *parameters) {
                                                  __Dictionary *giftDict = dynamic_cast<__Dictionary *>(parameters->objectForKey("gift"));
                                                  CCGift *gift = CCGift::createWithDictionary(giftDict);
                                                  this->onGiftSendFinished(gift);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GIFT_SEND_FAILED,
                                              [this](__Dictionary *parameters) {
                                                  __Dictionary *giftDict = dynamic_cast<__Dictionary *>(parameters->objectForKey("gift"));
                                                  CCGift *gift = CCGift::createWithDictionary(giftDict);
                                                  __String *errorMessage = dynamic_cast<__String *>(parameters->objectForKey("errorMessage"));
                                                  
                                                  this->onGiftSendFailed(gift, errorMessage);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GIFT_HAND_OUT_SUCCESS,
                                              [this](__Dictionary *parameters) {
                                                  __Dictionary *giftDict = dynamic_cast<__Dictionary *>(parameters->objectForKey("gift"));
                                                  CCGift *gift = CCGift::createWithDictionary(giftDict);
                                                  this->onGiftHandOutSuccess(gift);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GIFT_HAND_OUT_FAILED,
                                              [this](__Dictionary *parameters) {
                                                  __Dictionary *giftDict = dynamic_cast<__Dictionary *>(parameters->objectForKey("gift"));
                                                  CCGift *gift = CCGift::createWithDictionary(giftDict);
                                                  __String *errorMessage = dynamic_cast<__String *>(parameters->objectForKey("errorMessage"));
                                                  
                                                  this->onGiftHandOutFailed(gift, errorMessage);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_STARTED,
                                              [this](__Dictionary *parameters) {
                                                  __Integer *providerId = dynamic_cast<__Integer *>(parameters->objectForKey("providerId"));
                                                  this->onFetchFriendsStatesStarted(providerId->getValue());
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_FINISHED,
                                              [this](__Dictionary *parameters) {
                                                  __Integer *providerId = dynamic_cast<__Integer *>(parameters->objectForKey("providerId"));
                                                  
                                                  __Array *friendsStatesDicts = dynamic_cast<__Array *>(parameters->objectForKey("friendsStates"));
                                                  __Array *friendsStates = __Array::create();
                                                  
                                                  __Dictionary *friendStateDict = NULL;
                                                  for (unsigned int i = 0; i < friendsStatesDicts->count(); i++) {
                                                      friendStateDict = dynamic_cast<__Dictionary *>(friendsStatesDicts->getObjectAtIndex(i));
                                                      CC_ASSERT(friendStateDict);
                                                      
                                                      CCFriendState *friendState = CCFriendState::createWithDictionary(friendStateDict);
                                                      friendsStates->addObject(friendState);
                                                  }
                                                  
                                                  this->onFetchFriendsStatesFinished(providerId->getValue(), friendsStates);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_FAILED,
                                              [this](__Dictionary *parameters) {
                                                  __Integer *providerId = dynamic_cast<__Integer *>(parameters->objectForKey("providerId"));
                                                  __String *errorMessage = dynamic_cast<__String *>(parameters->objectForKey("errorMessage"));
                                                  
                                                  this->onFetchFriendsStatesFailed(providerId->getValue(), errorMessage);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GROW_DLC_INITIALIZED,
                                              [this](__Dictionary *parameters) {
                                                  this->onGrowDLCInitialized();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_DLC_PACKAGE_STATUS_UPDATED,
                                              [this](__Dictionary *parameters) {
                                                  __Bool *hasChanges = dynamic_cast<__Bool *>(parameters->objectForKey("hasChanges"));
                                                  __Array *packagesToSync = dynamic_cast<__Array *>(parameters->objectForKey("packagesToSync"));
                                                  __Array *packagesDeleted = dynamic_cast<__Array *>(parameters->objectForKey("packagesDeleted"));
                                                  this->onDLCPackagesStatusUpdate(hasChanges->getValue(), packagesToSync, packagesDeleted);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_STARTED,
                                              [this](__Dictionary *parameters) {
                                                  __String *packageId = dynamic_cast<__String *>(parameters->objectForKey("packageId"));
                                                  this->onDLCPackageSyncStarted(packageId);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_FINISHED,
                                              [this](__Dictionary *parameters) {
                                                  __String *packageId = dynamic_cast<__String *>(parameters->objectForKey("packageId"));
                                                  
                                                  this->onDLCPackageSyncFinished(packageId);
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_FAILED,
                                              [this](__Dictionary *parameters) {
                                                  __String *packageId = dynamic_cast<__String *>(parameters->objectForKey("packageId"));
                                                  __Integer *errorCode = dynamic_cast<__Integer *>(parameters->objectForKey("errorCode"));
                                                  __String *errorMessage = dynamic_cast<__String *>(parameters->objectForKey("errorMessage"));
                                                  this->onDLCPackageSyncFailed(packageId, CCDLCSyncError(errorCode->getValue()), errorMessage);
                                              });
        
        // Grow Insights
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_GROW_INSIGHTS_INITIALIZED,
                                              [this](__Dictionary *parameters) {
                                                  this->onGrowInsightsInitialized();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_INSIGHTS_REFRESH_STARTED,
                                              [this](__Dictionary *parameters) {
                                                  this->onInsightsRefreshStarted();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_INSIGHTS_REFRESH_FINISHED,
                                              [this](__Dictionary *parameters) {
                                                  this->onInsightsRefreshFinished();
                                              });
        
        eventDispatcher->registerEventHandler(CCHighwayConsts::EVENT_INSIGHTS_REFRESH_FAILED,
                                              [this](__Dictionary *parameters) {
                                                  this->onInsightsRefreshFailed();
                                              });
        
        return true;
    }

    void CCHighwayEventDispatcher::onGrowSyncInitialized() {
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GROW_SYNC_INITIALIZED);
    }
    
    void CCHighwayEventDispatcher::onModelSyncStarted() {
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_MODEL_SYNC_STARTED);
    }

    void CCHighwayEventDispatcher::onModelSyncFinished(cocos2d::__Array *changedComponents) {
        for (int i = 0; i < changedComponents->count(); ++i) {
            __String *componentName = dynamic_cast<__String *>(changedComponents->getObjectAtIndex(i));
            if ((componentName != NULL) && (componentName->compare("store") == 0)) {
                // Update local meta-data for store if updated
                SL_CREATE_PARAMS_FOR_METHOD(params, "Reflection::CCStoreInfo::initializeFromDB");
                CCSoomlaEventDispatcher::getInstance()->ndkCallback(params);
                break;
            }
        }
        
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(changedComponents, CCHighwayConsts::DICT_ELEMENT_MODEL_CHANGED_COMPONENTS);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_MODEL_SYNC_FINISHED, eventData);
    }

    void CCHighwayEventDispatcher::onModelSyncFailed(CCModelSyncError errorCode, cocos2d::__String *errorMessage) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(__Integer::create(errorCode), CCHighwayConsts::DICT_ELEMENT_ERROR_CODE);
        eventData->setObject(errorMessage, CCHighwayConsts::DICT_ELEMENT_ERROR_MESSAGE);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_MODEL_SYNC_FAILED, eventData);
    }

    void CCHighwayEventDispatcher::onStateSyncStarted() {
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_STATE_SYNC_STARTED);
    }

    void CCHighwayEventDispatcher::onStateSyncFinished(cocos2d::__Array *changedComponents, cocos2d::__Array *failedComponents) {
        for (int i = 0; i < changedComponents->count(); ++i) {
            __String *componentName = dynamic_cast<__String *>(changedComponents->getObjectAtIndex(i));
            if ((componentName != NULL) && (componentName->compare("store") == 0)) {
                // Update local state for store if updated
                SL_CREATE_PARAMS_FOR_METHOD(params, "Reflection::CCStoreInventory::refreshLocalInventory");
                CCSoomlaEventDispatcher::getInstance()->ndkCallback(params);
                break;
            }
        }
        
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(changedComponents, CCHighwayConsts::DICT_ELEMENT_STATE_CHANGED_COMPONENTS);
        eventData->setObject(failedComponents, CCHighwayConsts::DICT_ELEMENT_STATE_FAILED_COMPONENTS);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_STATE_SYNC_FINISHED, eventData);
    }

    void CCHighwayEventDispatcher::onStateSyncFailed(CCStateSyncError errorCode, cocos2d::__String *errorMessage) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(__Integer::create(errorCode), CCHighwayConsts::DICT_ELEMENT_ERROR_CODE);
        eventData->setObject(errorMessage, CCHighwayConsts::DICT_ELEMENT_ERROR_MESSAGE);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_STATE_SYNC_FAILED, eventData);
    }

    void CCHighwayEventDispatcher::onStateResetStarted() {
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_STATE_RESET_STARTED);
    }

    void CCHighwayEventDispatcher::onStateResetFinished() {
        // Update local state for store if updated
        SL_CREATE_PARAMS_FOR_METHOD(params, "Reflection::CCStoreInventory::refreshLocalInventory");
        CCSoomlaEventDispatcher::getInstance()->ndkCallback(params);
        
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_STATE_RESET_FINISHED);
    }

    void CCHighwayEventDispatcher::onStateResetFailed(CCStateSyncError errorCode, cocos2d::__String *errorMessage) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(__Integer::create(errorCode), CCHighwayConsts::DICT_ELEMENT_ERROR_CODE);
        eventData->setObject(errorMessage, CCHighwayConsts::DICT_ELEMENT_ERROR_MESSAGE);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_STATE_RESET_FAILED, eventData);
    }

    void CCHighwayEventDispatcher::onGrowGiftingInitialized() {
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GROW_GIFTING_INITIALIZED);
    }

    void CCHighwayEventDispatcher::onGiftsRetrieveStarted() {
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GIFTS_RETRIEVE_STARTED);
    }

    void CCHighwayEventDispatcher::onGiftsRetrieveFinished(cocos2d::__Array *retrievedGifts) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(retrievedGifts, CCHighwayConsts::DICT_ELEMENT_GIFTING_GIVEN_GIFTS);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GIFTS_RETRIEVE_FINISHED, eventData);
    }

    void CCHighwayEventDispatcher::onGiftsRetrieveFailed(cocos2d::__String *errorMessage) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(errorMessage, CCHighwayConsts::DICT_ELEMENT_GIFTING_ERROR_MESSAGE);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GIFTS_RETRIEVE_FAILED, eventData);
    }

    void CCHighwayEventDispatcher::onGiftSendStarted(CCGift *gift) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(gift, CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GIFT_SEND_STARTED, eventData);
    }

    void CCHighwayEventDispatcher::onGiftSendFinished(CCGift *gift) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(gift, CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GIFT_SEND_FINISHED, eventData);
    }

    void CCHighwayEventDispatcher::onGiftSendFailed(CCGift *gift, cocos2d::__String *errorMessage) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(gift, CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT);
        eventData->setObject(errorMessage, CCHighwayConsts::DICT_ELEMENT_GIFTING_ERROR_MESSAGE);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GIFT_SEND_FAILED, eventData);
    }

    void CCHighwayEventDispatcher::onGiftHandOutSuccess(CCGift *gift) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(gift, CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GIFT_HAND_OUT_SUCCESS, eventData);
    }

    void CCHighwayEventDispatcher::onGiftHandOutFailed(CCGift *gift, cocos2d::__String *errorMessage) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(gift, CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT);
        eventData->setObject(errorMessage, CCHighwayConsts::DICT_ELEMENT_GIFTING_ERROR_MESSAGE);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GIFT_HAND_OUT_FAILED, eventData);
    }

    void CCHighwayEventDispatcher::onFetchFriendsStatesStarted(int providerId) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(__Integer::create(providerId), CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_PROVIDER_ID);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_STARTED, eventData);
    }

    void CCHighwayEventDispatcher::onFetchFriendsStatesFinished(int providerId, cocos2d::__Array *friendsStates) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(__Integer::create(providerId), CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_PROVIDER_ID);
        eventData->setObject(friendsStates, CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_FRIENDS_STATES);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_FINISHED, eventData);
    }

    void CCHighwayEventDispatcher::onFetchFriendsStatesFailed(int providerId, cocos2d::__String *errorMessage) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(__Integer::create(providerId), CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_PROVIDER_ID);
        eventData->setObject(errorMessage, CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_ERROR_MESSAGE);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_FAILED, eventData);
    }

    void CCHighwayEventDispatcher::onGrowDLCInitialized() {
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GROW_DLC_INITIALIZED);
    }

    void CCHighwayEventDispatcher::onDLCPackagesStatusUpdate(bool hasChanges, cocos2d::__Array *packagesToSync, cocos2d::__Array *packagesDeleted) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(__Bool::create(hasChanges), CCHighwayConsts::DICT_ELEMENT_HAS_CHANGES);
        if (packagesToSync != NULL) {
            eventData->setObject(packagesToSync, CCHighwayConsts::DICT_ELEMENT_PACKAGES_TO_SYNC);
        }
        if (packagesDeleted != NULL) {
            eventData->setObject(packagesDeleted, CCHighwayConsts::DICT_ELEMENT_PACKAGES_DELETED);
        }
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_DLC_PACKAGE_STATUS_UPDATED, eventData);
    }

    void CCHighwayEventDispatcher::onDLCPackageSyncStarted(cocos2d::__String *packageId) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(packageId, CCHighwayConsts::DICT_ELEMENT_DLC_PACKAGE_ID);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_STARTED, eventData);
    }

    void CCHighwayEventDispatcher::onDLCPackageSyncFinished(cocos2d::__String *packageId) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(packageId, CCHighwayConsts::DICT_ELEMENT_DLC_PACKAGE_ID);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_FINISHED, eventData);
    }

    void CCHighwayEventDispatcher::onDLCPackageSyncFailed(cocos2d::__String *packageId, CCDLCSyncError errorCode, cocos2d::__String *errorMessage) {
        __Dictionary *eventData = __Dictionary::create();
        eventData->setObject(packageId, CCHighwayConsts::DICT_ELEMENT_DLC_PACKAGE_ID);
        eventData->setObject(__Integer::create(errorCode), CCHighwayConsts::DICT_ELEMENT_DLC_ERROR_CODE);
        eventData->setObject(errorMessage, CCHighwayConsts::DICT_ELEMENT_DLC_ERROR);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_FAILED, eventData);
    }
    
    // Grow Insights
    
    void CCHighwayEventDispatcher::onGrowInsightsInitialized() {
        CCGrowInsights::getInstance()->i_syncWithNative();
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_GROW_INSIGHTS_INITIALIZED);
    }
    
    void CCHighwayEventDispatcher::onInsightsRefreshStarted() {
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_INSIGHTS_REFRESH_STARTED);
    }
    
    void CCHighwayEventDispatcher::onInsightsRefreshFinished() {
        CCGrowInsights::getInstance()->i_syncWithNative();
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_INSIGHTS_REFRESH_FINISHED);
    }
    
    void CCHighwayEventDispatcher::onInsightsRefreshFailed() {
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(CCHighwayConsts::EVENT_INSIGHTS_REFRESH_FAILED);
    }

}
