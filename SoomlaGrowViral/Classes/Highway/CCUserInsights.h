/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCUserInsights_H_
#define __CCUserInsights_H_

#include "cocos2d.h"
#include "CCSoomlaMacros.h"
#include "CCDomain.h"
#include "CCHighwayConsts.h"
#include "CCPayInsights.h"

namespace grow {
    
    /**
     @class CCUserInsights
     @brief Represents insights for a user
     
     Inheritance CCUserInsights > CCDomain > cocos2d::Ref
     */
    class CCUserInsights : public soomla::CCDomain {
        SL_SYNTHESIZE_RETAIN_WITH_DICT_DCL(CCPayInsights *, mPayInsights, PayInsights);
    public:
        /**
         Constructor
         
         Main constructor for the user insights which nullifies all information in
         the class
         */
        CCUserInsights() : CCDomain(), mPayInsights(NULL) {};
        
        SL_CREATE_WITH_DICTIONARY(CCUserInsights);
        
        /**
         Initializes the class instance with information provided in a
         dictionary format with keys corresponding to fields JSON constants
         (see SL_SYNTHESIZE_RETAIN_WITH_DICT macros above)
         */
        virtual bool initWithDictionary(cocos2d::__Dictionary *dict);
        
        /**
         Converts the user insights instance into a dictionary using the fields'
         JSON constants as keys
         (see SL_SYNTHESIZE_RETAIN_WITH_DICT macros above)
         */
        virtual cocos2d::__Dictionary *toDictionary();
        
        /**
         Destructor for the user insights
         */
        virtual ~CCUserInsights();
    };
    
}


#endif // __CCUserInsights_H_
