/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCHighwayEventDispatcher_H_
#define __CCHighwayEventDispatcher_H_

#include "cocos2d.h"
#include "CCGift.h"

namespace grow {
    
    /**
     Enumeration of model sync failure codes
     */
    enum CCModelSyncError {
        MODEL_GENERAL_ERROR = 0,
        MODEL_SERVER_ERROR = 1,
        MODEL_UPDATE_MODEL_ERROR = 2
    };
    
    /**
     Enumeration of state sync failure codes
     */
    enum CCStateSyncError {
        STATE_GENERAL_ERROR = 0,
        STATE_SERVER_ERROR = 1,
        STATE_UPDATE_STATE_ERROR = 2
    };
    
    /**
     Enumeration of DLC sync failure codes
     */
    enum CCDLCSyncError {
        DLC_GENERAL_ERROR = 0,
        DLC_DOWNLOAD_ERROR = 1,
        DLC_DELETE_ERROR = 2,
        DLC_SERVER_ERROR = 3
    };
    
    /**
     @class CCHighwayEventDispatcher
     @brief Fires event when recieved from the native implementation.
     
     Signs up to native Highway events.
     When the events arrive this class fires the repective event through
     the Cocos2dx Event Dispatcher.
     */
    class CCHighwayEventDispatcher : public cocos2d::Ref {
    public:
        /**
         This class is singleton, access it with this function.
         */
        static CCHighwayEventDispatcher *getInstance();
        
        /**
         Initializes the event dispatcher after construction
         */
        bool init();
        
        /**
         Fired when Grow Sync is intialized.
         
         Event Name - CCHighwayConsts::EVENT_GROW_SYNC_INITIALIZED
         */
        virtual void onGrowSyncInitialized();
        /**
         Fired when the model sync process has began.
         
         Event Name - CCHighwayConsts::EVENT_MODEL_SYNC_STARTED
         */
        virtual void onModelSyncStarted();
        /**
         Fired when the model sync process has finished.
         
         Event Name - CCHighwayConsts::EVENT_MODEL_SYNC_FINISHED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_MODEL_CHANGED_COMPONENTS - __Array -
         a List of modules' names (`cocos2d::__String`) which were synced.
         */
        virtual void onModelSyncFinished(cocos2d::__Array *changedComponents);
        /**
         Fired when the model sync process has failed.
         
         Event Name - CCHighwayConsts::EVENT_MODEL_SYNC_FAILED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_ERROR_CODE - CCModelSyncError -
         The error code of the failure
         CCHighwayConsts::DICT_ELEMENT_ERROR_MESSAGE - __String - The reason 
         why the process failed
         */
        virtual void onModelSyncFailed(CCModelSyncError errorCode, cocos2d::__String *errorMessage);
        /**
         Fired when the state sync process has began.
         
         Event Name - CCHighwayConsts::EVENT_STATE_SYNC_STARTED
         */
        virtual void onStateSyncStarted();
        /**
         Fired when the state sync process has finished.
         
         Event Name - CCHighwayConsts::EVENT_STATE_SYNC_FINISHED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_STATE_CHANGED_COMPONENTS - __Array - 
         a List of modules' names (`cocos2d::__String`) which were updated.
         CCHighwayConsts::DICT_ELEMENT_STATE_FAILED_COMPONENTS - __Array - 
         a List of modules' names (`cocos2d::__String`) which failed to update.
         */
        virtual void onStateSyncFinished(cocos2d::__Array *changedComponents, cocos2d::__Array *failedComponents);
        /**
         Fired when the state sync process has failed.
         
         Event Name - CCHighwayConsts::EVENT_META_SYNC_FAILED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_ERROR_CODE - CCStateSyncError -
         The error code of the failure
         CCHighwayConsts::DICT_ELEMENT_ERROR_MESSAGE - __String - The reason
         why the process failed
         */
        virtual void onStateSyncFailed(CCStateSyncError errorCode, cocos2d::__String *errorMessage);
        /**
         Fired when the state reset process has began.
         
         Event Name - CCHighwayConsts::EVENT_STATE_RESET_STARTED
         */
        virtual void onStateResetStarted();
        /**
         Fired when the state reset process has finished.
         
         Event Name - CCHighwayConsts::EVENT_STATE_RESET_FINISHED
         */
        virtual void onStateResetFinished();
        /**
         Fired when the state reset process has failed.
         
         Event Name - CCHighwayConsts::EVENT_STATE_RESET_FAILED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_ERROR_CODE - CCStateSyncError -
         The error code of the failure
         CCHighwayConsts::DICT_ELEMENT_ERROR_MESSAGE - __String - The reason
         why the process failed
         */
        virtual void onStateResetFailed(CCStateSyncError errorCode, cocos2d::__String *errorMessage);
        /**
         Fired when Grow Gifting is intialized.
         
         Event Name - CCHighwayConsts::EVENT_GROW_GIFTING_INITIALIZED
         */
        virtual void onGrowGiftingInitialized();
        /**
         Fired when gifting has started retrieving a list of gifts for the user.
         
         Event Name - CCHighwayConsts::EVENT_GIFTS_RETRIEVE_STARTED
         */
        virtual void onGiftsRetrieveStarted();
        /**
         Fired when the list of gifts for the user has been retrieved.
         NOTE: This event is fired just before the gifts are handed out
         to the user
         
         Event Name - CCHighwayConsts::EVENT_GIFTS_RETRIEVE_FINISHED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_GIFTING_GIVEN_GIFTS - __Array -
         a List of gifts (`CCGift`) which will be handed
         */
        virtual void onGiftsRetrieveFinished(cocos2d::__Array *retrievedGifts);
        /**
         Fired when gifting failed to retrive a list of gifts for the user.
         
         Event Name - CCHighwayConsts::EVENT_GIFTS_RETRIEVE_FAILED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_GIFTING_ERROR_MESSAGE - __String -
         The reason why the retrieval failed
         */
        virtual void onGiftsRetrieveFailed(cocos2d::__String *errorMessage);
        /**
         Fired when a gift has began to be sent to the server.
         
         Event Name - CCHighwayConsts::EVENT_GIFT_SEND_STARTED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT - CCGift -
         the gift that is being sent.
         */
        virtual void onGiftSendStarted(CCGift *gift);
        /**
         Fired when sending the gift has failed.
         NOTE: At this point the gift will have an ID
         
         Event Name - CCHighwayConsts::EVENT_GIFT_SEND_FINISHED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT - CCGift -
         the gift which was sent.
         */
        virtual void onGiftSendFinished(CCGift *gift);
        /**
         Fired when sending the gift has failed.
         
         Event Name - CCHighwayConsts::EVENT_GIFT_SEND_FAILED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT - CCGift -
         the gift has failed to be sent.
         CCHighwayConsts::DICT_ELEMENT_GIFTING_ERROR_MESSAGE - __String -
         The reason why the gift has failed to be sent.
         */
        virtual void onGiftSendFailed(CCGift *gift, cocos2d::__String *errorMessage);
        /**
         Fired when the gift was handed out to the user.
         
         Event Name - CCHighwayConsts::EVENT_GIFT_HAND_OUT_SUCCESS
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT - CCGift - the gift which 
         was handed out to the user.
         */
        virtual void onGiftHandOutSuccess(CCGift *gift);
        /**
         Fired when handing out the gift to the user has failed.
         
         Event Name - CCHighwayConsts::EVENT_GIFT_HAND_OUT_FAILED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT - CCGift -
         the gift that failed to be handed out.
         CCHighwayConsts::DICT_ELEMENT_GIFTING_ERROR_MESSAGE - __String -
         The reason why the gift has failed to be handed out.
         */
        virtual void onGiftHandOutFailed(CCGift *gift, cocos2d::__String *errorMessage);
        /**
         Fired when grow leaderboards starts fetching friends' states for a
         specific social provider.
         
         Event Name - CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_STARTED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_PROVIDER_ID - __Integer -
         The social provider ID for which the fetch operation is preformed.
         */
        virtual void onFetchFriendsStatesStarted(int providerId);
        /**
         Fired when grow leaderboards finished fetching friends' states for a
         specific social provider.
         
         Event Name - CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_FINISHED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_PROVIDER_ID - __Integer -
         The social provider ID for which the fetch operation is preformed.
         CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_FRIENDS_STATES - __Array -
         a List of friends' states for the requested query.
         */
        virtual void onFetchFriendsStatesFinished(int providerId, cocos2d::__Array *friendsStates);
        /**
         Fired when grow leaderboards fails to fetch friends' states for a specific
         social provider.
         
         Event Name - CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_FAILED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_PROVIDER_ID - __Integer -
         The social provider ID for which the fetch operation is preformed.
         CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_ERROR_MESSAGE - __String -
         The error message explaining why the fetch operation failed
         */
        virtual void onFetchFriendsStatesFailed(int providerId, cocos2d::__String *errorMessage);
        /**
         Fired when the Grow DLC client is initialized.
         
         Event Name - CCHighwayConsts::EVENT_GROW_DLC_INITIALIZED
         */
        virtual void onGrowDLCInitialized();
        /**
         Fired when a package/s update status check has returned from the server.
         
         Event Name - CCHighwayConsts::EVENT_DLC_PACKAGE_STATUS_UPDATED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_HAS_CHANGES - __Bool - General status 
         against the server state of packages.
         CCHighwayConsts::DICT_ELEMENT_PACKAGES_TO_SYNC - __Array - All packages 
         which were updated on the server and need to be synced on the device.
         The error message explaining why the query operation failed
         CCHighwayConsts::DICT_ELEMENT_PACKAGES_DELETED - __Array - All packages 
         which were deleted from the device as a result of their deletion from 
         the server.
         */
        virtual void onDLCPackagesStatusUpdate(bool hasChanges, cocos2d::__Array *packagesToSync, cocos2d::__Array *packagesDeleted);
        /**
         This event is fired when the package starts the syncing process
         
         Event Name - CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_STARTED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_DLC_PACKAGE_ID - __String - The package 
         started syncing
         */
        virtual void onDLCPackageSyncStarted(cocos2d::__String *packageId);
        /**
         This event is fired when the package finishes the syncing process
         
         Event Name - CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_FINISHED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_DLC_PACKAGE_ID - __String - The package
         finished syncing
         */
        virtual void onDLCPackageSyncFinished(cocos2d::__String *packageId);
        /**
         This event is fired when the package failed the syncing process.
         
         Event Name - CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_FAILED
         Event Data (__Dictionary):
         CCHighwayConsts::DICT_ELEMENT_DLC_PACKAGE_ID - __String - The package 
         which failed to sync
         CCHighwayConsts::DICT_ELEMENT_DLC_ERROR_CODE - CCDLCSyncError - 
         The error code for this failure
         CCHighwayConsts::DICT_ELEMENT_DLC_ERROR - __String - The error message 
         for the failure
         */
        virtual void onDLCPackageSyncFailed(cocos2d::__String *packageId, CCDLCSyncError errorCode, cocos2d::__String *errorMessage);
        
        
        /**************** Grow Insights ****************/
        
        /**
         Fired when Grow Insights is intialized.
         
         Event Name - CCHighwayConsts::EVENT_GROW_INSIGHTS_INITIALIZED
         */
        virtual void onGrowInsightsInitialized();
        /**
         Fired when Grow insights has started refreshing insights about the user.
         
         Event Name - CCHighwayConsts::EVENT_INSIGHTS_REFRESH_STARTED
         */
        virtual void onInsightsRefreshStarted();
        /**
         Fired when Grow insights has finished refreshing insights about the user.
         
         Event Name - CCHighwayConsts::EVENT_INSIGHTS_REFRESH_FINISHED
         */
        virtual void onInsightsRefreshFinished();
        /**
         Fired when Grow insights has failed refreshing insights about the user.
         
         Event Name - CCHighwayConsts::EVENT_INSIGHTS_REFRESH_FAILED
         */
        virtual void onInsightsRefreshFailed();
    };
}

#endif // __CCHighwayEventDispatcher_H_
