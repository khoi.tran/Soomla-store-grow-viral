/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __Cocos2dxHighway_H
#define __Cocos2dxHighway_H

#include "CCGrowHighway.h"
#include "CCGrowSync.h"
#include "CCGift.h"
#include "CCGiftPayload.h"
#include "CCGrowGifting.h"
#include "CCFriendState.h"
#include "CCGrowLeaderboards.h"
#include "CCGrowDLC.h"
#include "CCGrowInsights.h"
#include "CCHighwayConsts.h"
#include "CCHighwayEventDispatcher.h"

using namespace grow;

#endif /* !__Cocos2dxHighway_H */
