/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCHighwayConsts_H_
#define __CCHighwayConsts_H_

namespace grow {
    
    /**
     @class CCHighwayConsts
     @brief Provides all the consts used by the Highway project
     
     The string consts defined here are used to define a common interface
     between string consts used in the native version of Highway and the`
     Cocos2dx one.
     This includes JSON field names and events published by native versions.
     */
    class CCHighwayConsts {
    public:
        // Sync events
        static char const *DICT_ELEMENT_ERROR_CODE;
        static char const *DICT_ELEMENT_ERROR_MESSAGE;
        static char const *DICT_ELEMENT_MODEL_CHANGED_COMPONENTS;
        static char const *DICT_ELEMENT_STATE_CHANGED_COMPONENTS;
        static char const *DICT_ELEMENT_STATE_FAILED_COMPONENTS;
        
        static char const *INTERNAL_EVENT_ON_STATE_CONFLICT;
        static char const *EVENT_GROW_SYNC_INITIALIZED;
        static char const *EVENT_MODEL_SYNC_STARTED;
        static char const *EVENT_MODEL_SYNC_FINISHED;
        static char const *EVENT_MODEL_SYNC_FAILED;
        static char const *EVENT_STATE_SYNC_STARTED;
        static char const *EVENT_STATE_SYNC_FINISHED;
        static char const *EVENT_STATE_SYNC_FAILED;
        static char const *EVENT_STATE_RESET_STARTED;
        static char const *EVENT_STATE_RESET_FINISHED;
        static char const *EVENT_STATE_RESET_FAILED;
        
        // Gift JSON
        static char const *JSON_GIFT_ID;
        static char const *JSON_GIFT_FROM_UID;
        static char const *JSON_GIFT_TO_PROVIDER;
        static char const *JSON_GIFT_TO_PROFILE_ID;
        static char const *JSON_GIFT_PAYLOAD;
        static char const *JSON_GIFT_PAYLOAD_ASSOCIATED_ITEM_ID;
        static char const *JSON_GIFT_PAYLOAD_ITEMS_AMOUNT;
        
        // Gifting events
        static char const *DICT_ELEMENT_GIFTING_GIVEN_GIFTS;
        static char const *DICT_ELEMENT_GIFTING_GIFT;
        static char const *DICT_ELEMENT_GIFTING_ERROR_MESSAGE;
        
        static char const *EVENT_GROW_GIFTING_INITIALIZED;
        static char const *EVENT_GIFTS_RETRIEVE_STARTED;
        static char const *EVENT_GIFTS_RETRIEVE_FINISHED;
        static char const *EVENT_GIFTS_RETRIEVE_FAILED;
        static char const *EVENT_GIFT_SEND_STARTED;
        static char const *EVENT_GIFT_SEND_FINISHED;
        static char const *EVENT_GIFT_SEND_FAILED;
        static char const *EVENT_GIFT_HAND_OUT_SUCCESS;
        static char const *EVENT_GIFT_HAND_OUT_FAILED;
        
        // FriendState JSON
        static char const *JSON_FRIEND_PROFILE_ID;
        static char const *JSON_FRIEND_RECORDS;
        static char const *JSON_FRIEND_LAST_COMPLETED_WORLDS;
        
        // Query events
        static char const *DICT_ELEMENT_LEADERBOARDS_PROVIDER_ID;
        static char const *DICT_ELEMENT_LEADERBOARDS_FRIENDS_STATES;
        static char const *DICT_ELEMENT_LEADERBOARDS_ERROR_MESSAGE;
        
        static char const *EVENT_FETCH_FRIENDS_STATES_STARTED;
        static char const *EVENT_FETCH_FRIENDS_STATES_FINISHED;
        static char const *EVENT_FETCH_FRIENDS_STATES_FAILED;
        
        // DLC events
        static char const *DICT_ELEMENT_DLC_PACKAGE_ID;
        static char const *DICT_ELEMENT_DLC_ERROR;
        static char const *DICT_ELEMENT_DLC_ERROR_CODE;
        static char const *DICT_ELEMENT_HAS_CHANGES;
        static char const *DICT_ELEMENT_PACKAGES_TO_SYNC;
        static char const *DICT_ELEMENT_PACKAGES_DELETED;
        
        static char const *EVENT_GROW_DLC_INITIALIZED;
        static char const *EVENT_DLC_PACKAGE_STATUS_UPDATED;
        static char const *EVENT_DLC_PACKAGE_SYNC_STARTED;
        static char const *EVENT_DLC_PACKAGE_SYNC_FINISHED;
        static char const *EVENT_DLC_PACKAGE_SYNC_FAILED;
        
        // Insights JSON
        static char const *JSON_PAY_INSIGHTS;
        static char const *JSON_PAY_RANK_BY_GENRE;
        
        // Insights events
        static char const *EVENT_GROW_INSIGHTS_INITIALIZED;
        static char const *EVENT_INSIGHTS_REFRESH_STARTED;
        static char const *EVENT_INSIGHTS_REFRESH_FINISHED;
        static char const *EVENT_INSIGHTS_REFRESH_FAILED;
    };
}

#endif // __CCHighwayConsts_H_
