/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCGrowHighway_H_
#define __CCGrowHighway_H_


#include "cocos2d.h"

namespace grow {

    /**
     @class CCGrowHighway
     @brief The main connector to the SOOMLA Highway server
     */
    class CCGrowHighway : public cocos2d::Ref {
    public:
        /**
         This class is singleton, access it with this function.
         */
        static CCGrowHighway *getInstance();

        /**
         Initializes the singleton instance of `CCGrowHighway`
         
         @param gameKey The game's unique key (received from the Grow dashboard)
         @param envKey The game's unique environment key (received from the Grow dashboard)
         */
        static void initShared(cocos2d::__String *gameKey, cocos2d::__String *envKey, cocos2d::__String *highwayUrl = NULL, cocos2d::__String *servicesUrl = NULL);

        /**
         See `initShared`
         */
        virtual bool init(cocos2d::__String *gameKey, cocos2d::__String *envKey, cocos2d::__String *highwayUrl = NULL, cocos2d::__String *servicesUrl = NULL);
        
    private:
        CCGrowHighway();
        void bindNative();
    };
}

#endif //__CCGrowHighway_H_
