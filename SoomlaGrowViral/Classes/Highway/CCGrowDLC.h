/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCGrowDLC_H_
#define __CCGrowDLC_H_

#include "cocos2d.h"

namespace grow {
    
    /**
     @class CCGrowDLC
     @brief This class provides DLC functionality to the highway package.
     This mainly means contacting the server and synchronizing asset packages
     defined by the game developer (including downloading and deleting).
     */
    class CCGrowDLC : public cocos2d::Ref {
    public:
        /**
         This class is singleton, access it with this function.
         */
        static CCGrowDLC *getInstance();
        
        /**
         Initializes the DLC manager, this method will check for updates on
         all the synced packages on the device and return a state via
         the `onDLCPackagesStatusUpdate` handler.
         */
        static void initShared();
        
        /**
         See `initShared`
         */
        virtual bool init();
        
        /**
         Checks the status of a single package, the result will be returned via
         `onDLCPackagesStatusUpdate` handler.
         @param packageId The ID of a defined package for the game to check
         */
        virtual void checkPackageStatus(const char *packageId);
        /**
         Checks all synced packages' status against the server, the result will
         be returned via `onDLCPackagesStatusUpdate` handler.
         Deleted packages from the server will be deleted from the device.
         */
        virtual void checkSyncedPackagesStatus();
        /**
         Starts synchronizing the provided DLC package (only one sync at a time)
         @param packageId The ID of a defined package for the game
         @return true if the syncing process has started, false otherwise
         */
        virtual bool startSync(const char *packageId);
        /**
         Get a full path to a file in a package
         @param packageId The ID of a defined package for the game
         @param filename The filename which may or may not exist in the package
         @return a Full path for the supplied filename in a package, which might
         not exist on the device itself
         */
        virtual const char *getPathToFile(const char *packageId, const char *filename);
        /**
         Gets a list of full paths to files in a package
         @param packageId The ID of a defined package for the game
         @return a List of full paths to files currently in the package
         */
        virtual cocos2d::__Array *getListOfFiles(const char *packageId);
    };
    
    
}

#endif // __CCGrowDLC_H_
