/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCUserInsights.h"

bool grow::CCUserInsights::initWithDictionary(cocos2d::__Dictionary *dict) {
    fillPayInsightsFromDict(dict);
    
    return true;
}

void grow::CCUserInsights::fillPayInsightsFromDict(cocos2d::__Dictionary *dict) {
    cocos2d::__Dictionary *payInsightsDict = dynamic_cast<cocos2d::__Dictionary *>(dict->objectForKey(CCHighwayConsts::JSON_PAY_INSIGHTS));
    CC_ASSERT(payInsightsDict);
    
    CCPayInsights *payInsights = CCPayInsights::createWithDictionary(payInsightsDict);
    setPayInsights(payInsights);
}

cocos2d::__Dictionary *grow::CCUserInsights::toDictionary() {
    cocos2d::__Dictionary* dict = cocos2d::__Dictionary::create();
    
    putPayInsightsToDict(dict);
    
    return dict;
}

void grow::CCUserInsights::putPayInsightsToDict(cocos2d::__Dictionary *dict) {
    dict->setObject(getPayInsights()->toDictionary(), CCHighwayConsts::JSON_PAY_INSIGHTS);
}

grow::CCUserInsights::~CCUserInsights() {
    CC_SAFE_RELEASE(mPayInsights);
}