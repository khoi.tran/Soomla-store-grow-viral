/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCGrowInsights.h"
#include "CCNdkBridge.h"
#include "CCSoomlaUtils.h"
#include "CCSoomlaMacros.h"
#include "CCUserInsights.h"

USING_NS_CC;

using namespace soomla;
namespace grow {

#define TAG "SOOMLA CCGrowInsights"
    
    static CCGrowInsights *sInstance = nullptr;
    CCUserInsights *mUserInsights = nullptr;
    
    CCGrowInsights *CCGrowInsights::getInstance() {
        if (!sInstance)
        {
            sInstance = new CCGrowInsights();
            sInstance->retain();
        }
        return sInstance;
    }
    
    void CCGrowInsights::initShared() {
        CCGrowInsights *growInsights = CCGrowInsights::getInstance();
        if (!growInsights->init()) {
            exit(1);
        }
    }
    
    bool CCGrowInsights::init() {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowInsights::init");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call init failed with error: %s", error->getInfo())->getCString());
            return false;
        }
        
        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);
        
        return ret->getValue();
    }
    
    bool CCGrowInsights::refreshInsights() {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowInsights::refreshInsights");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call refresh insights failed with error: %s", error->getInfo())->getCString());
            return false;
        }
        
        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);
        
        return ret->getValue();
    }
    
    bool CCGrowInsights::i_syncWithNative() {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowInsights::getUserInsights");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call refresh insights failed with error: %s", error->getInfo())->getCString());
            return false;
        }
        
        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);
        
        if (ret->getValue()) {
            mUserInsights = CCUserInsights::createWithDictionary(dynamic_cast<__Dictionary *>(retParams->objectForKey("userInsights")));
            return true;
        }
        
        return ret->getValue();
    }
    
    CCUserInsights *CCGrowInsights::getUserInsights() {
        return mUserInsights;
    }
    
    CCGrowInsights::CCGrowInsights() {
    }
}
