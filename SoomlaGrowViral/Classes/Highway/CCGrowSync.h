/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCGrowSync_H_
#define __CCGrowSync_H_

#include "cocos2d.h"

namespace grow {
    
    /**
     @class CCGrowSync
     @brief Represents a class which is in charge of syncing model and state
     between the client and the server.
     */
    class CCGrowSync : public cocos2d::Ref {
    public:
        /**
         This class is singleton, access it with this function.
         */
        static CCGrowSync *getInstance();
        
        /**
         Initialize Grow Sync module
         @param modelSync Should Grow Sync synchronize model for
         integrated modules
         @param stateSync Should Grow Sync synchronize state for integrated
         modules
         */
        static void initShared(bool modelSync, bool stateSync);
        
        /**
         See `initShared`
         */
        virtual bool init(bool modelSync, bool stateSync);
        
        /**
         Resets the state for all integrated modules, locally and remotely
         */
        virtual bool resetState();
        
        /**
         Called internally by `CCHighwayEventDispatcher` to resolve state
         conflicts using the provided custom conflict resolver 
         (see `setStateConflictResolver`)
         */
        virtual bool resolveStateConflict(cocos2d::__Dictionary *remoteState, cocos2d::__Dictionary *currentState,
                                                            cocos2d::__Dictionary *stateDiff);
        
        /**
         Sets a custom state conflict resolver, it will be called when there are
         conflicts between client and server.
         @param customStateConflictResolver a Function which will resolve the 
         conflict between the provided states.
         
         The function will have the following parameters:
         remoteState - the state on the server
         currentState - the state currently on the client
         stateDiff - the difference between the remote and local state in an
         easier way to parse
         Should return the state that should be used to resolve the conflict
         */
        void setStateConflictResolver(std::function<cocos2d::__Dictionary*(cocos2d::__Dictionary*, cocos2d::__Dictionary*, cocos2d::__Dictionary*)> customStateConflictResolver);
        
    private:
        CCGrowSync();
        
        std::function<cocos2d::__Dictionary*(cocos2d::__Dictionary*, cocos2d::__Dictionary*, cocos2d::__Dictionary*)> resolver;
    };
    
    
}

#endif //__CCGrowSync_H_
