/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCGiftPayload_H_
#define __CCGiftPayload_H_

#include "cocos2d.h"
#include "CCSoomlaMacros.h"
#include "CCDomain.h"
#include "CCHighwayConsts.h"

namespace grow {

    /**
     @class CCGiftPayload
     @brief Represents a gift payload which provides information of actual 
     gift value
     
     Inheritance CCGiftPayload > CCDomain > cocos2d::Ref
     */
    class CCGiftPayload : public soomla::CCDomain {
        SL_SYNTHESIZE_RETAIN_WITH_DICT(cocos2d::__String *, mAssociatedItemId, AssociatedItemId, CCHighwayConsts::JSON_GIFT_PAYLOAD_ASSOCIATED_ITEM_ID);
        SL_SYNTHESIZE_RETAIN_WITH_DICT(cocos2d::__Integer *, mItemsAmount, ItemsAmount, CCHighwayConsts::JSON_GIFT_PAYLOAD_ITEMS_AMOUNT);
    public:
        /**
         Constructor
         
         Main constructor for the gift payload which nullifies all information in
         the class
         */
        CCGiftPayload() : CCDomain(), mAssociatedItemId(NULL), mItemsAmount(NULL) {};
        
        /**
         Creates an instance of `CCGiftPayload` according to the information
         provided
         
         @param associatedItemId The associated virtual item ID to give as a 
         part of the gifting process
         @param itemsAmount How much of associated virtual item should be 
         given as a part of the gifting process
         @return the created instance
         */
        static CCGiftPayload *create(cocos2d::__String *associatedItemId,
                                     cocos2d::__Integer *itemsAmount);
        
        SL_CREATE_WITH_DICTIONARY(CCGiftPayload);
        
        /**
         Initializes the class instance with the provided information
         @param associatedItemId The associated virtual item ID to give as a
         part of the gifting process.
         @param itemsAmount How much of associated virtual item should be
         given as a part of the gifting process.
         @return true if the initialization was successful, false otherwise
         */
        virtual bool init(cocos2d::__String *associatedItemId,
                          cocos2d::__Integer *itemsAmount);
        
        /**
         Initializes the class instance with information provided in a
         dictionary format with keys corresponding to fields JSON constants
         (see SL_SYNTHESIZE_RETAIN_WITH_DICT macros above)
         */
        virtual bool initWithDictionary(cocos2d::__Dictionary *dict);
        
        /**
         Converts the gift payload instance into a dictionary using the fields'
         JSON constants as keys
         (see SL_SYNTHESIZE_RETAIN_WITH_DICT macros above)
         */
        virtual cocos2d::__Dictionary *toDictionary();
        
        /**
         Destructor for the gift
         */
        virtual ~CCGiftPayload();
    };
}

#endif // __CCGiftPayload_H_
