/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCGiftPayload.h"

grow::CCGiftPayload *grow::CCGiftPayload::create(cocos2d::__String *associatedItemId,
                                              cocos2d::__Integer *itemsAmount) {
    
    CCGiftPayload *ret = new CCGiftPayload();
    if (ret->init(associatedItemId, itemsAmount)) {
        ret->autorelease();
    }
    else {
        CC_SAFE_DELETE(ret);
    }
    return ret;
}

bool grow::CCGiftPayload::init(cocos2d::__String *associatedItemId,
                                 cocos2d::__Integer *itemsAmount) {
    
    setAssociatedItemId(associatedItemId);
    setItemsAmount(itemsAmount);
    
    return true;
}

bool grow::CCGiftPayload::initWithDictionary(cocos2d::__Dictionary *dict) {
    
    fillAssociatedItemIdFromDict(dict);
    fillItemsAmountFromDict(dict);
    
    return true;
}

cocos2d::__Dictionary *grow::CCGiftPayload::toDictionary() {
    cocos2d::__Dictionary* dict = cocos2d::__Dictionary::create();
    
    putAssociatedItemIdToDict(dict);
    putItemsAmountToDict(dict);
    
    return dict;
}

grow::CCGiftPayload::~CCGiftPayload() {
    CC_SAFE_RELEASE(mAssociatedItemId);
    CC_SAFE_RELEASE(mItemsAmount);
}