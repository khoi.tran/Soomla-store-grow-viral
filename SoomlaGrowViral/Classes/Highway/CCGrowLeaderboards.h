/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCGrowLeaderboards_H_
#define __CCGrowLeaderboards_H_

#include "cocos2d.h"

namespace grow {
    
    /**
     @class CCGrowLeaderboards
     @brief Represents a manager class which is in charge of querying information from
     the server.
     */
    class CCGrowLeaderboards : public cocos2d::Ref {
    public:
        /**
         Fetches the friends' state from the server.
         The friends' state contains relevant information on completed levels
         and highscores for the provided list of users.
         @param providerId The social provider ID for which to get the friends' 
         state
         @param friendsProfileIds a List of friends' profile IDs in the social
         network provided
         @return True if the operation has started, false if it cannot
         */
        static bool fetchFriendsStates(int providerId, cocos2d::__Array *friendsProfileIds);
    };
}

#endif //__CCGrowLeaderboards_H_
