/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCPayInsights_H_
#define __CCPayInsights_H_

#include "cocos2d.h"
#include "CCSoomlaMacros.h"
#include "CCDomain.h"
#include "CCHighwayConsts.h"

namespace grow {
    
    /**
     Represents a game genre
     */
    enum Genre {
        Action,
        Adventure,
        Arcade,
        Board,
        Card,
        Casino,
        Casual,
        Educational,
        Family,
        Music,
        Puzzle,
        Racing,
        Role_Playing,
        Simulation,
        Sports,
        Strategy,
        Trivia,
        Word
    };
    
    /**
     @class CCPayInsights
     @brief Represents pay insights for a user
     
     Inheritance CCPayInsights > CCDomain > cocos2d::Ref
     */
    class CCPayInsights : public soomla::CCDomain {
        SL_SYNTHESIZE_RETAIN_WITH_DICT(cocos2d::__Dictionary *, mPayRankByGenre, PayRankByGenre, CCHighwayConsts::JSON_PAY_RANK_BY_GENRE);
    public:
        /**
         Constructor
         
         Main constructor for pay insights which nullifies all information in
         the class
         */
        CCPayInsights() : CCDomain(), mPayRankByGenre(NULL) {};
        
        SL_CREATE_WITH_DICTIONARY(CCPayInsights);
        
        /**
         Initializes the class instance with information provided in a
         dictionary format with keys corresponding to fields JSON constants
         (see SL_SYNTHESIZE_RETAIN_WITH_DICT macros above)
         */
        virtual bool initWithDictionary(cocos2d::__Dictionary *dict);
        
        /**
         Converts the pay insights instance into a dictionary using the fields'
         JSON constants as keys
         (see SL_SYNTHESIZE_RETAIN_WITH_DICT macros above)
         */
        virtual cocos2d::__Dictionary *toDictionary();
        
        /**
         Returns the pay rank of this user according to genre
         @param genre The genre to use.
         @return pay rank of this user if applicable to genre, -1 otherwise
         */
        virtual int getPayRankForGenre(Genre genre);
        
        /**
         Destructor for the pay insights
         */
        virtual ~CCPayInsights();
    };
    
}


#endif // __CCPayInsights_H_
