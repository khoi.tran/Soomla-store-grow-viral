/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCGrowHighway.h"
#include "CCNdkBridge.h"
#include "CCSoomlaUtils.h"
#include "CCSoomlaMacros.h"
#include "CCHighwayEventDispatcher.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <string>

#define CLASS_NAME "com/soomla/cocos2dx/highway/HighwayBridgeBinder"
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#include "CCHighwayBridgeBinderIos.h"

#endif

USING_NS_CC;

using namespace soomla;
namespace grow {

#define TAG "SOOMLA CCGrowHighway"

    static CCGrowHighway *sInstance = nullptr;

    CCGrowHighway *CCGrowHighway::getInstance() {
        if (!sInstance)
        {
            sInstance = new CCGrowHighway();
            sInstance->retain();
        }
        return sInstance;
    }

    void CCGrowHighway::initShared(cocos2d::__String *gameKey, cocos2d::__String *envKey, __String *highwayUrl, __String *servicesUrl) {
        
        CCHighwayEventDispatcher::getInstance();    // to get sure it's inited
        
        CCGrowHighway *growHighway = CCGrowHighway::getInstance();
        if (!growHighway->init(gameKey, envKey, highwayUrl, servicesUrl)) {
            exit(1);
        }
    }

    bool CCGrowHighway::init(cocos2d::__String *gameKey, cocos2d::__String *envKey, __String *highwayUrl, __String *servicesUrl) {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowHighway::init");
        params->setObject(gameKey, "gameKey");
        params->setObject(envKey, "envKey");
        
        if (highwayUrl) {
            params->setObject(highwayUrl, "highwayUrl");
        }
        
        if (servicesUrl) {
            params->setObject(servicesUrl, "servicesUrl");
        }

        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);

        if (error) {
            CCSoomlaUtils::logError(TAG, __String::createWithFormat(
                    "call init failed with error: %s", error->getInfo())->getCString());
            return false;
        }

        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);

        return ret->getValue();
    }

    CCGrowHighway::CCGrowHighway() {
        // Just bind to native before initing
        this->bindNative();
    }
    
    void CCGrowHighway::bindNative() {
        CCSoomlaUtils::logDebug(TAG, "Binding to native platform Highway bridge...");
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        JniMethodInfo minfo;
        
        bool exists = JniHelper::getStaticMethodInfo(minfo, CLASS_NAME, "bind", "()V");
        
        if (exists)
        {
            minfo.env->CallStaticVoidMethod(minfo.classID, minfo.methodID);
        }
        else {
            CCSoomlaUtils::logError(TAG, "Unable to bind native Highway bridge on Android");
        }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        soomla::CCHighwayBridgeBinderIos::bind();
#endif
    }
}
