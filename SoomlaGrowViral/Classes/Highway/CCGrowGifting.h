/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCGrowGifting_H_
#define __CCGrowGifting_H_

#include "cocos2d.h"

namespace grow {
    
    /**
     @class CCGrowGifting
     @brief Represents a manager class which is in charge of sending and 
     receiving gifts between users playing the same game.
     */
    class CCGrowGifting : public cocos2d::Ref {
    public:
        /**
         This class is singleton, access it with this function.
         */
        static CCGrowGifting *getInstance();
        
        /**
         Initializes the gifting manager
         */
        static void initShared();
        
        /**
         See `initShared`
         */
        virtual bool init();
        
        /**
         Sends a gift from the currently logged in user (with Profile) to 
         the provided user.
         This method gives the gift from the game without affecting the player.
         @param toProvider The social provider ID of the user to send gift to.
         @param toProfileId The social provider user ID to send gift to.
         @param itemId The virtual item ID to give as a gift.
         @param amount The amount of virtual items to gift.
         @return false if the operation cannot be started, true otherwise.
         */
        virtual bool sendGift(int toProvider, const char *toProfileId, const char *itemId, int amount);
        
        /**
         Sends a gift from the currently logged in user (with Profile) to
         the provided user.
         @param toProvider The social provider ID of the user to send gift to.
         @param toProfileId The social provider user ID to send gift to.
         @param itemId The virtual item ID to give as a gift.
         @param amount The amount of virtual items to gift.
         @param deductFromUser Should the virtual items be deducted from the
         player upon sending the gift
         @return false if the operation cannot be started, true otherwise.
         */
        virtual bool sendGift(int toProvider, const char *toProfileId, const char *itemId, int amount, bool deductFromUser);
        
    private:
        CCGrowGifting();
    };
    
    
}

#endif // __CCGrowGifting_H_
