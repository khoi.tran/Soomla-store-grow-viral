/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCGrowSync.h"
#include "CCNdkBridge.h"
#include "CCSoomlaUtils.h"
#include "CCSoomlaMacros.h"

USING_NS_CC;

using namespace soomla;
namespace grow {
    
#define TAG "SOOMLA CCGrowSync"
    
    static CCGrowSync *sInstance = nullptr;
    
    CCGrowSync *CCGrowSync::getInstance() {
        if (!sInstance)
        {
            sInstance = new CCGrowSync();
            sInstance->retain();
        }
        return sInstance;
    }
    
    void CCGrowSync::initShared(bool modelSync, bool stateSync) {
        CCGrowSync *growSync = CCGrowSync::getInstance();
        if (!growSync->init(modelSync, stateSync)) {
            exit(1);
        }
    }
    
    bool CCGrowSync::init(bool modelSync, bool stateSync) {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowSync::init");
        params->setObject(__Bool::create(modelSync), "modelSync");
        params->setObject(__Bool::create(stateSync), "stateSync");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call init failed with error: %s", error->getInfo())->getCString());
            return false;
        }
        
        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);
        
        return ret->getValue();
    }
    
    bool CCGrowSync::resetState() {
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowSync::resetState");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call reset state failed with error: %s", error->getInfo())->getCString());
            return false;
        }
        
        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);
        
        return ret->getValue();
    }
    
    bool CCGrowSync::resolveStateConflict(cocos2d::__Dictionary *remoteState, cocos2d::__Dictionary *currentState,
                                                cocos2d::__Dictionary *stateDiff) {
        
        __Dictionary *resolvedState = resolver(remoteState, currentState, stateDiff);
        
        int conflictResolveStrategy = 2;
        if (resolvedState == remoteState) {
            conflictResolveStrategy = 0;
        } else if (resolvedState == currentState) {
            conflictResolveStrategy = 1;
        }
        
        SL_CREATE_PARAMS_FOR_METHOD(params, "CCGrowSync::resolveConflictCallback");
        params->setObject(__Integer::create(conflictResolveStrategy), "conflictResolveStrategy");
        params->setObject(resolvedState, "resolvedState");
        
        CCError *error = NULL;
        __Dictionary *retParams = (__Dictionary *) CCNdkBridge::callNative (params, &error);
        
        if (error) {
            CCSoomlaUtils::logError(TAG,
                                    __String::createWithFormat("call resolveConflictCallback failed with error: %s", error->getInfo())->getCString());
        }
        
        SL_EXTRACT_FROM_RETURN(__Bool, ret, retParams);
        
        return ret->getValue();
    }
    
    void CCGrowSync::setStateConflictResolver(std::function<cocos2d::__Dictionary*(cocos2d::__Dictionary*, cocos2d::__Dictionary*, cocos2d::__Dictionary*)> customStateConflictResolver) {
        resolver = customStateConflictResolver;
    }
    
    CCGrowSync::CCGrowSync() {
        resolver = [this](__Dictionary *remoteState, __Dictionary *currentState, __Dictionary *stateDiff) {
            return remoteState;
        };
    }
}