/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCHighwayConsts.h"

namespace grow {
    char const *CCHighwayConsts::DICT_ELEMENT_ERROR_CODE = "sync_error_code";
    char const *CCHighwayConsts::DICT_ELEMENT_ERROR_MESSAGE = "sync_error_message";
    char const *CCHighwayConsts::DICT_ELEMENT_MODEL_CHANGED_COMPONENTS = "model_sync_changed_components";
    char const *CCHighwayConsts::DICT_ELEMENT_STATE_CHANGED_COMPONENTS = "state_sync_changed_components";
    char const *CCHighwayConsts::DICT_ELEMENT_STATE_FAILED_COMPONENTS = "state_sync_failed_components";
    
    char const *CCHighwayConsts::INTERNAL_EVENT_ON_STATE_CONFLICT = "CCHighwayEventDispatcher::onStateConflict";
    
    char const *CCHighwayConsts::EVENT_GROW_SYNC_INITIALIZED = "com.soomla.sync.events.GrowSyncInitializedEvent";
    char const *CCHighwayConsts::EVENT_MODEL_SYNC_STARTED = "com.soomla.sync.events.ModelSyncStartedEvent";
    char const *CCHighwayConsts::EVENT_MODEL_SYNC_FINISHED = "com.soomla.sync.events.ModelSyncFinishedEvent";
    char const *CCHighwayConsts::EVENT_MODEL_SYNC_FAILED = "com.soomla.sync.events.ModelSyncFailedEvent";
    char const *CCHighwayConsts::EVENT_STATE_SYNC_STARTED = "com.soomla.sync.events.StateSyncStartedEvent";
    char const *CCHighwayConsts::EVENT_STATE_SYNC_FINISHED = "com.soomla.sync.events.StateSyncFinishedEvent";
    char const *CCHighwayConsts::EVENT_STATE_SYNC_FAILED = "com.soomla.sync.events.StateSyncFailedEvent";
    char const *CCHighwayConsts::EVENT_STATE_RESET_STARTED = "com.soomla.sync.events.StateResetStartedEvent";
    char const *CCHighwayConsts::EVENT_STATE_RESET_FINISHED = "com.soomla.sync.events.StateResetFinishedEvent";
    char const *CCHighwayConsts::EVENT_STATE_RESET_FAILED = "com.soomla.sync.events.StateResetFailedEvent";
    
    char const *CCHighwayConsts::JSON_GIFT_ID = "giftId";
    char const *CCHighwayConsts::JSON_GIFT_FROM_UID = "fromUid";
    char const *CCHighwayConsts::JSON_GIFT_TO_PROVIDER = "toProvider";
    char const *CCHighwayConsts::JSON_GIFT_TO_PROFILE_ID = "toProfileId";
    char const *CCHighwayConsts::JSON_GIFT_PAYLOAD = "payload";
    char const *CCHighwayConsts::JSON_GIFT_PAYLOAD_ASSOCIATED_ITEM_ID = "associatedItemId";
    char const *CCHighwayConsts::JSON_GIFT_PAYLOAD_ITEMS_AMOUNT = "itemsAmount";
    
    char const *CCHighwayConsts::DICT_ELEMENT_GIFTING_GIVEN_GIFTS = "givenGifts";
    char const *CCHighwayConsts::DICT_ELEMENT_GIFTING_GIFT = "gift";
    char const *CCHighwayConsts::DICT_ELEMENT_GIFTING_ERROR_MESSAGE = "errorMessage";
    
    char const *CCHighwayConsts::EVENT_GROW_GIFTING_INITIALIZED = "com.soomla.gifting.events.GrowGiftingInitializedEvent";
    char const *CCHighwayConsts::EVENT_GIFTS_RETRIEVE_STARTED = "com.soomla.gifting.events.GiftsRetrieveStartedEvent";
    char const *CCHighwayConsts::EVENT_GIFTS_RETRIEVE_FINISHED = "com.soomla.gifting.events.GiftsRetrieveFinishedEvent";
    char const *CCHighwayConsts::EVENT_GIFTS_RETRIEVE_FAILED = "com.soomla.gifting.events.GiftsRetrieveFailedEvent";
    char const *CCHighwayConsts::EVENT_GIFT_SEND_STARTED = "com.soomla.gifting.events.GiftSendStartedEvent";
    char const *CCHighwayConsts::EVENT_GIFT_SEND_FINISHED = "com.soomla.gifting.events.GiftSendFinishedEvent";
    char const *CCHighwayConsts::EVENT_GIFT_SEND_FAILED = "com.soomla.gifting.events.GiftSendFailedEvent";
    char const *CCHighwayConsts::EVENT_GIFT_HAND_OUT_SUCCESS = "com.soomla.gifting.events.GiftHandOutSuccessEvent";
    char const *CCHighwayConsts::EVENT_GIFT_HAND_OUT_FAILED = "com.soomla.gifting.events.GiftHandOutFailedEvent";
    
    char const *CCHighwayConsts::JSON_FRIEND_PROFILE_ID = "profileId";
    char const *CCHighwayConsts::JSON_FRIEND_RECORDS = "records";
    char const *CCHighwayConsts::JSON_FRIEND_LAST_COMPLETED_WORLDS = "lastCompletedWorlds";
    
    char const *CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_PROVIDER_ID = "providerId";
    char const *CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_FRIENDS_STATES = "friendsStates";
    char const *CCHighwayConsts::DICT_ELEMENT_LEADERBOARDS_ERROR_MESSAGE = "errorMessage";
    
    char const *CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_STARTED = "com.soomla.leaderboards.events.FetchFriendsStatesStartedEvent";
    char const *CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_FINISHED = "com.soomla.leaderboards.events.FetchFriendsStatesFinishedEvent";
    char const *CCHighwayConsts::EVENT_FETCH_FRIENDS_STATES_FAILED = "com.soomla.leaderboards.events.FetchFriendsStatesFailedEvent";
    
    char const *CCHighwayConsts::DICT_ELEMENT_DLC_PACKAGE_ID = "packageId";
    char const *CCHighwayConsts::DICT_ELEMENT_DLC_ERROR = "errorMessage";
    char const *CCHighwayConsts::DICT_ELEMENT_DLC_ERROR_CODE = "errorCode";
    char const *CCHighwayConsts::DICT_ELEMENT_HAS_CHANGES = "hasChanges";
    char const *CCHighwayConsts::DICT_ELEMENT_PACKAGES_TO_SYNC = "packagesToSync";
    char const *CCHighwayConsts::DICT_ELEMENT_PACKAGES_DELETED = "packagesDeleted";
    
    char const *CCHighwayConsts::EVENT_GROW_DLC_INITIALIZED = "com.soomla.dlc.events.GrowDLCInitializedEvent";
    char const *CCHighwayConsts::EVENT_DLC_PACKAGE_STATUS_UPDATED = "com.soomla.dlc.events.DLCPackagesStatusUpdateEvent";
    char const *CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_STARTED = "com.soomla.dlc.events.DLCPackageSyncStartedEvent";
    char const *CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_FINISHED = "com.soomla.dlc.events.DLCPackageSyncFinishedEvent";
    char const *CCHighwayConsts::EVENT_DLC_PACKAGE_SYNC_FAILED = "com.soomla.dlc.events.DLCPackageSyncFailedEvent";
    
    char const *CCHighwayConsts::JSON_PAY_INSIGHTS = "pay";
    char const *CCHighwayConsts::JSON_PAY_RANK_BY_GENRE = "payRankByGenre";
    
    char const *CCHighwayConsts::EVENT_GROW_INSIGHTS_INITIALIZED = "com.soomla.insights.events.GrowInsightsInitialized";
    char const *CCHighwayConsts::EVENT_INSIGHTS_REFRESH_STARTED = "com.soomla.insights.events.InsightsRefreshStarted";
    char const *CCHighwayConsts::EVENT_INSIGHTS_REFRESH_FINISHED = "com.soomla.insights.events.InsightsRefreshFinished";
    char const *CCHighwayConsts::EVENT_INSIGHTS_REFRESH_FAILED = "com.soomla.insights.events.InsightsRefreshFailed";
}