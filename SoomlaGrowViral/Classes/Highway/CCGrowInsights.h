/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCGrowInsights_H_
#define __CCGrowInsights_H_


#include "cocos2d.h"
#include "CCUserInsights.h"

namespace grow {

    /**
     @class CCGrowInsights
     @brief Represents a manager class which is in charge of fetching insights.
     */
    class CCGrowInsights : public cocos2d::Ref {
    public:
        /**
         This class is singleton, access it with this function.
         */
        static CCGrowInsights *getInstance();

        /**
         Initializes the insights manager
         */
        static void initShared();
        
        /**
         See `initShared`
         */
        virtual bool init();
    
        /**
         Refresh GrowInsights with SOOMLA GROW
         */
        virtual bool refreshInsights();
        
        /**
         Sync the internal state with native
         */
        virtual bool i_syncWithNative();
        
        /**
         Get user insights
         */
        virtual CCUserInsights *getUserInsights();
    private:
        CCGrowInsights();
    };
}

#endif //__CCGrowInsights_H_
