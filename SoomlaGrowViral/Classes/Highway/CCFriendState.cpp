/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#include "CCFriendState.h"

USING_NS_CC;


namespace grow {

    CCFriendState *CCFriendState::create(cocos2d::__String *profileId,
                                         cocos2d::__Dictionary *records,
                                         cocos2d::__Dictionary *lastCompletedWorlds) {
        
        CCFriendState *ret = new CCFriendState();
        if (ret->init(profileId, records, lastCompletedWorlds)) {
            ret->autorelease();
        }
        else {
            CC_SAFE_DELETE(ret);
        }
        return ret;
    }

    bool CCFriendState::init(cocos2d::__String *profileId,
                             cocos2d::__Dictionary *records,
                             cocos2d::__Dictionary *lastCompletedWorlds) {
        
        setProfileId(profileId);
        setRecords(records);
        setLastCompletedWorlds(lastCompletedWorlds);
        
        return true;
    }

    bool CCFriendState::initWithDictionary(cocos2d::__Dictionary *dict) {
        fillProfileIdFromDict(dict);
        fillRecordsFromDict(dict);
        fillLastCompletedWorldsFromDict(dict);
        
        // This piece of code is here to make sure that the records are a map
        // between a score and its DOUBLE value.
        // This is needed only since the Jansson reads a number value as int
        // if it has no decimal point.
        // It should be removed when a normal JSON lib is going to be used.
        __Dictionary *doubleValuedRecords = __Dictionary::create();
        DictElement *recordElement;
        CCDICT_FOREACH(mRecords, recordElement) {
            __Double *doubleValue = dynamic_cast<__Double *>(recordElement->getObject());
            if (doubleValue == NULL) {
                __Integer *integerValue = dynamic_cast<__Integer *>(recordElement->getObject());
                if (integerValue != NULL) {
                    doubleValue = __Double::create(integerValue->getValue());
                }
                else {
                    doubleValue = __Double::create(0.0);
                }
            }
            const char *key = recordElement->getStrKey();
            doubleValuedRecords->setObject(doubleValue, key);
        }
        setRecords(doubleValuedRecords);
        
        return true;
    }

    cocos2d::__Dictionary *CCFriendState::toDictionary() {
        cocos2d::__Dictionary* dict = cocos2d::__Dictionary::create();
        
        putProfileIdToDict(dict);
        putRecordsToDict(dict);
        putLastCompletedWorldsToDict(dict);
        
        return dict;
    }

    CCFriendState::~CCFriendState() {
        CC_SAFE_RELEASE(mProfileId);
        CC_SAFE_RELEASE(mRecords);
        CC_SAFE_RELEASE(mLastCompletedWorlds);
    }

}