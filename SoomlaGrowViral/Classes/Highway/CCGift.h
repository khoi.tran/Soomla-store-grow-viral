/*
 * Copyright (C) 2012-2015 Soomla Inc. - All Rights Reserved
 *
 *   Unauthorized copying of this file, via any medium is strictly prohibited
 *   Proprietary and confidential
 *
 *   Written by Refael Dakar <refael@soom.la>
 */

#ifndef __CCGift_H_
#define __CCGift_H_

#include "cocos2d.h"
#include "CCSoomlaMacros.h"
#include "CCDomain.h"
#include "CCHighwayConsts.h"
#include "CCGiftPayload.h"

namespace grow {
    
    /**
     @class CCGift
     @brief Represents a gift from one user to the other
     
     Inheritance CCGift > CCDomain > cocos2d::Ref
     */
    class CCGift : public soomla::CCDomain {
        SL_SYNTHESIZE_RETAIN_WITH_DICT(cocos2d::__String *, mId, Id, CCHighwayConsts::JSON_GIFT_ID);
        SL_SYNTHESIZE_RETAIN_WITH_DICT(cocos2d::__String *, mFromUid, FromUid, CCHighwayConsts::JSON_GIFT_FROM_UID);
        SL_SYNTHESIZE_RETAIN_WITH_DICT(cocos2d::__Integer *, mToProvider, ToProvider, CCHighwayConsts::JSON_GIFT_TO_PROVIDER);
        SL_SYNTHESIZE_RETAIN_WITH_DICT(cocos2d::__String *, mToProfileId, ToProfileId, CCHighwayConsts::JSON_GIFT_TO_PROFILE_ID);
        SL_SYNTHESIZE_RETAIN_WITH_DICT_DCL(CCGiftPayload *, mPayload, Payload);
    public:
        /**
         Constructor
         
         Main constructor for the gift which nullifies all information in 
         the class
         */
        CCGift() : CCDomain(), mId(NULL), mFromUid(NULL), mToProvider(NULL) , mToProfileId(NULL),
        mPayload(NULL) {};
        
        /**
         Creates an instance of `CCGift` according to the information
         provided
         
         @param giftId The Gift's ID, only when received from server.
         @param fromUid User's UID from which the gift originated.
         @param toProvider Social provider ID of the user to which the gift 
         was sent.
         @param toProfileId The receiving user's ID on the provider social 
         provider.
         @param payload Payload for the gift.
         @return the created instance
         */
        static CCGift *create(cocos2d::__String *giftId,
                              cocos2d::__String *fromUid,
                              cocos2d::__Integer *toProvider,
                              cocos2d::__String *toProfileId,
                              CCGiftPayload *payload);
        
        SL_CREATE_WITH_DICTIONARY(CCGift);
        
        /**
         Initializes the class instance with the provided information
         @param giftId The Gift's ID, only when received from server.
         @param fromUid User's UID from which the gift originated.
         @param toProvider Social provider ID of the user to which the gift
         was sent.
         @param toProfileId The receiving user's ID on the provider social
         provider.
         @param payload Payload for the gift.
         @return true if the initialization was successful, false otherwise
         */
        virtual bool init(
                          cocos2d::__String *giftId,
                          cocos2d::__String *fromUid,
                          cocos2d::__Integer *toProvider,
                          cocos2d::__String *toProfileId,
                          CCGiftPayload *payload);
        
        /**
         Initializes the class instance with information provided in a
         dictionary format with keys corresponding to fields JSON constants
         (see SL_SYNTHESIZE_RETAIN_WITH_DICT macros above)
         */
        virtual bool initWithDictionary(cocos2d::__Dictionary *dict);
        
        /**
         Converts the gift instance into a dictionary using the fields'
         JSON constants as keys
         (see SL_SYNTHESIZE_RETAIN_WITH_DICT macros above)
         */
        virtual cocos2d::__Dictionary *toDictionary();
        
        /**
         Destructor for the gift
         */
        virtual ~CCGift();
    };
    
}


#endif // __CCGift_H_
