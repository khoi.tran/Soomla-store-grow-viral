/*
 * Copyright (C) 2012 Soomla Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "AppDelegate.h"
#include "MuffinRushAssets.h"
#include "MainScene.h"
#include "ExampleEventHandler.h"
#include "Cocos2dxStore.h"
#include "HelloWorldScene.h"
#include "Cocos2dxHighway.h"
#include "CCSoomlaProfile.h"

USING_NS_CC;

AppDelegate::AppDelegate() {
}

AppDelegate::~AppDelegate()
{
}

bool AppDelegate::applicationDidFinishLaunching() {


	//Set store event listener
	handler = new ExampleEventHandler();

	//Init soomla with secret key
	CCSoomla::initialize("darknova"); //-> like a password, to secure your data

	//Grow viral
	CCGrowHighway::initShared(
	        __String::create("f2e318f1-9579-4ea9-a342-ed8f65f79a40"), //"your game key"
	        __String::create("188c7f45-9971-490d-88f6-08710214de52")); //"your env key"
	 CCGrowGifting::initShared();

	//Init store
	MuffinRushAssets *assets = MuffinRushAssets::create();
	 __Dictionary *storeParams = __Dictionary::create();
	  storeParams->setObject(__String::create("ExamplePublicKey"), "androidPublicKey"); //-> key from market
	  storeParams->setObject(__Bool::create(true), "testPurchases"); //-> enable testing mode
	  storeParams->setObject(__Bool::create(false), "SSV"); //-> fraud protection, isn't in need now
	  storeParams->setObject(__Bool::create(false), "verifyOnServerFailure"); //-> fraud protection, isn't in need now
	 CCSoomlaStore::initialize(assets, storeParams);

	 //Init profile
	 __Dictionary *profileParams = __Dictionary::create();
	 soomla::CCSoomlaProfile::initialize(profileParams);

	 //Give user 5000 coins
    __Array *currencies = CCStoreInfo::sharedStoreInfo()->getCurrencies();
    Ref *currencyObject;
    CCARRAY_FOREACH(currencies, currencyObject) {
            CCVirtualCurrency *vc = dynamic_cast<CCVirtualCurrency *>(currencyObject);
			CCStoreInventory::sharedStoreInventory()->giveItem(vc->getItemId()->getCString(),5000, NULL);
	}



    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if COCOS2D_VERSION > 0x00030200
        glview = GLViewImpl::create("My Game");
#else
        glview = GLView::create("My Game");
#endif
        director->setOpenGLView(glview);
    }

    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
//    auto scene = MainScene::getMainScene();
    auto scene = HelloWorld::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
